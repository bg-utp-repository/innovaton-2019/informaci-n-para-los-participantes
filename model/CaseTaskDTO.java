package com.bgp.innovaton.rest.service.bgcase.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CaseTaskDTO {
  // Id de la Tarea asignada por el sistema
  protected String taskId;
  // Creador de la tarea
  protected String creatorNetworkUser;
  // Asignado a la tarea
  protected String asignedNetworkUser;
  // Fecha de Creación de la Tarea
  protected String createdDate;
  // Fecha de Completitud de la Tarea
  protected String completeDate;
}

package com.bgp.innovaton.rest.service.bgcase.model;


import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CaseDTO implements Serializable {

  private static final long serialVersionUID = -7988011601236670041L;
  // Id de identificación del caso asignado por el Sistema
  protected String caseId;
  // Objeto con la información del Caso según el tipo de caso
  protected CaseInformationDTO caseInformation;
  // Información de las etapas del caso que son requeridas para su solución segun el tipo de caso
  protected List<CaseStageDTO> stage;
  // Información de Contacto (telefonos, Correos, etc..)
  protected List<ContactDTO> contact;
  // Información de soluciones dadas al caso
  protected List<SolutionDTO> solutions;
  // Grupos de Solución asignados al Caso
  protected SolutionGroupDTO solutionGroup;
  // Notas dejadas al caso (pueden ser del cliente, el ejecutivo, o los grupos de solución que
  // atendieron el caso)
  protected List<NoteDTO> note;
  // Información de Auditoría, en el cual por lo menos deben estar las personas que realizaron
  // modificaciones al caso
  protected List<AuditTrackDTO> audit;
}

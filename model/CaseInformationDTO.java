package com.bgp.innovaton.rest.service.bgcase.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CaseInformationDTO {
  // Unidad de Negocio que registra el caso (Sucursal, Telemercadeo, CallCenter...)
  protected String businessUnit;
  //
  protected String origin;
  // Categoria/clasiicación del Caso
  protected String category;
  // Tipo de caso
  protected String caseType;
  // Información del Cliente
  protected CustomerDTO customer;
  // Producto afectado
  protected CaseProductDTO product;
  // Estado del Caso
  protected String state;
  // Fecha de Creación
  protected String createdDate;
  // Fecha de Solución
  protected String solutionDate;
  // Dias establecidos en el acuerdo de servicio para solucionar el tipo de caso
  protected int daysServiceLevelAgreement;
  // Prioridad del caso
  protected String priority;
  protected String caseResume;
  // Información en texto sobre el caso brindada por el cliente
  protected String assignedNetworkUser;
  // Usuario que tiene asignado el Caso en el momento
  protected String accountableNetworkUser;
  // Usuario que tiene asignado el Caso frente al Cliente
  protected String displayTemplateId;
  // Template asociado al tipo de caso para la captura de Información adicional según el tipo de
  // caso
  protected String resultAction;
  // Texto que describe las acciones realizadas para solucionar el caso



}

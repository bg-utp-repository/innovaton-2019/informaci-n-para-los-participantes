package com.bgp.innovaton.rest.service.bgcase.model;



import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SolutionDTO implements Serializable {

  private static final long serialVersionUID = -1L;
  private String solutionId;
  private String description;
  private String resume;
  private String createdDate;
  private String createdBy;
  private String finalSolution;
}

package com.bgp.innovaton.rest.service.bgcase.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CaseProductDTO {

  protected String productType;
  protected String accountNumber;
  protected String accountType;
  protected String cardNumber;

}

package com.bgp.innovaton.rest.service.bgcase.model;

public class ContactDTO {

  protected String contactMethod;
  protected String contactPerson;
  protected String cellPhone;
  protected String phone;
  protected String email;
  protected String twiter;
  protected String facebook;
}

Información para los participantes

# [BG-UTP] INNOVATÓN 2019

### [Entregables](Innovatón_Entregable_final_V6.pdf)

Documento con información del Reto y los entregables

### [Requerimientos](Requerimiento_1_1.pdf)

Documento con los requerimientos técnicos y funcionales

### [Model ](model)
Repositorio con las clases ejemplo del sistema de casos.
Obs.:  No se limite al contenido de este modelo, el mismo es solo referencial
